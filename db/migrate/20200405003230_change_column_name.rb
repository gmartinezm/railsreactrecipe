class ChangeColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :recipes, :instrucion, :instruction
  end
end
